﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTemplate.EF_Models;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using MVCTemplate.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace MVCTemplate.Controllers
{
    public class AccountsController : ApplicationBaseController
    {
        //Accounts

        TemplateEntities _sv = new TemplateEntities();
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountsController()
        {
        }
        public AccountsController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> Login(string returnUrl)
        {

            if (User.Identity.IsAuthenticated)
            {


                returnUrl = !String.IsNullOrEmpty(returnUrl) ? returnUrl : "/Home/Index";
                return RedirectToLocal(returnUrl);

            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        //[CaptchaMvc.Attributes.CaptchaVerify("Mã bảo mật nhập không đúng")]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe,
                    false);
                if (result == SignInStatus.Success)
                    if (string.IsNullOrEmpty(returnUrl))
                        returnUrl = "/Home/Index";
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, model.RememberMe });
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không đúng.");
                        return View(model);
                }
            }
            catch (Exception ex)
            {
                var x = ex.Message;
                return View(model);
            }

        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return Json(data: 1);
        }
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }



        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("oops") : View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {

                return View();
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                //return RedirectToAction("ResetPassword", "Account");
                ModelState.AddModelError("", "Địa chỉ Email không tồn tại");
                TempData["Error"] = "Địa chỉ Email không tồn tại.";
                return View();
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "TaiKhoan");
            }
            AddErrors(result);
            return View();
        }
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }
        // GET: Accounts
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult GetList()
        {
            try
            {
                var data = _sv.AspNetUsers.Select(p=> new {
                    p.Id,
                    p.UserName,
                    p.Email
                }).ToList();
                var json = JsonConvert.SerializeObject(data);
                return Content(json, "application/json");
            }
            catch (Exception ex)
            {
                
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
           
        }
    }
}