/// <reference path="myController.js" />
var paggingConfig = {
    pageSize: 10,
    pageIndex: 1,
};
var myController = {
    init: function () {
        myController.loadData();
        myController.registerEvent();
    },
    registerEvent: function () {
        $("#btn_Search").off("click").on("click",
            function () {
                myController.loadData(true);
            });
        $(".tr_Data").off("click").on("click",
            function () {
                var id = $(this).data("id");
                $('.tr_Data').css('background-color', '#FFFFFF');
                $(this).css('background-color', '#8f9190');

                myController.LoadDetail(id);
            });
        $("#btn_New").off("click").on("click",
            function () {
                $('.tr_Data').css('background-color', '#FFFFFF');
                myController.resetForm();
            });

        $("#btn_Update").off("click").on("click",
            function () {
                myController.saveData();
                myController.resetForm();
                $("#input_id").val("");
            });
        // DELETE EVENT
        $("#btn_Delete").off("click").on("click", function () {
            bootbox.confirm({
                title: "Xác nhận xóa dữ liệu",
                message: "Bạn có chắc muốn xóa bản ghi được chọn?",
                buttons: {
                    confirm: {
                        label: '<i class="fa fa-check pull-right"></i> Đồng ý'
                    },

                    cancel: {
                        label: '<i class="fa fa-times pull-left"></i> Thoát'
                    }
                },
                callback: function (result) {
                    if (result == true) {
                        var id = $("#input_id").val();
                        myController.deleteData(id);
                        myController.loadData(true);
                        myController.resetForm();
                    }
                }
            });
        });


       
        $('#tableAccounts').on('click-row.bs.table', function (e, row, $element) {
          
            $('.success').removeClass('success');
            $($element).addClass('success');
        })

    },

    // DELETE BY ID FUNCTION
    deleteData: function (id) {
        $.ajax({
            url: "/DanhMucMonHoc/DeleteData",
            type: "POST",
            dataType: "json",
            data: {
                id: id
            },
            success: function (response) {
                if (response.status) {
                    bootbox.alert({
                        message: "Xóa dữ liệu thành công.",
                        backdrop: true
                    });
                }
            },
            error: function (err) {
                bootbox.alert({
                    message: response.message,
                    backdrop: true
                });
            }
        });
    },
    // RESET FUNCTION
    resetForm: function () {
        $("#input_id").val(0);
        $("#input_ten_mon_hoc").val("");

    },
    // LOAD DATA BY ID
    LoadDetail: function (id) {

        $.ajax({
            url: "/DanhMucMonHoc/GetDetailByID",
            type: "GET",
            data: {
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.status) {
                    var data = response.data;
                    $("#input_id").val(data.id);
                    $("#input_ten_mon_hoc").val(data.ten_mon_hoc);
                }
            }
        });
    },
    // CHANGE PAGE SIZE
    loadData: function () {
        $(function () {
            $.ajax({
                type: "POST",
                url: '../Accounts/GetList',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data.Message != null) {
                        //    toastr.error(data.Message, 'Thông báo');
                        return;
                    }
                    if (data.length > 0) {

                        //    toastr.success('Tìm kiếm thành công !', 'Thông báo');
                    }
                    else {
                        //    toastr.error('Không thấy kết quả !', 'Thông báo');
                    }

                    $("#tableAccounts").bootstrapTable('refreshOptions', { data });
                    }
                    });
        });
    },
    //SAVE TO DB FUNCTION
    saveData: function () {
        var id = $("#input_id").val(); //kiểu LONG ko phải int nha// dùng thư viện Bigint.min.js; 
        var ten_mon_hoc = $("#input_ten_mon_hoc").val();
        // var xxx = parseInt($('#input_xxx').val()); Nếu là kiểu số thì convert kiểu này
        var dantoc = {
            id: id,
            ten_mon_hoc: ten_mon_hoc,
        };
        $.ajax({
            url: "/DanhMucMonHoc/SaveData",
            type: "POST",
            dataType: "json",
            data: {
                str_JSON: JSON.stringify(dantoc)
            },
            success: function (response) {
                if (response.status == true) {
                    // Sau này đổi thành notification
                    bootbox.alert("Cập nhập thành công",
                        function () {
                            myController.loadData(true);
                        });
                } else {
                    bootbox.alert(response.message);
                }
            },
            error: function (err) {
                bootbox.alert(response.err);
            }
        });
    },
    //PAGING FUNCTION
    paging: function (totalRow, callback, changePageSize) {
        var totalPage = Math.ceil(totalRow / paggingConfig.pageSize);
        if ($("#pagination a").length === 0 || changePageSize === true) {
            $("#pagination").empty();
            $("#pagination").removeData("twbs-pagination");
            $("#pagination").unbind("page");
        }
        $("#pagination").twbsPagination({
            totalPages: totalPage,
            first: "Đầu",
            next: "Tiếp",
            last: "Cuối",
            prev: "Trước",
            visiblePages: 10,
            onPageClick: function (event, page) {
                paggingConfig.pageIndex = page;
                setTimeout(callback, 200);
            }
        });
    }
};

myController.init();